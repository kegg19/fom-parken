# FOM-Parken

## HowTo:
1. Ordner input erstellen und dort die PDF Dateien ablegen
2. Programm ausführen
3. Im Ordner output (wird erzeugt falls nicht vorhanden) werden die PDF Dateien im CSV Format abgelegt
4. Nun können die Daten bedeutend einfacher bspw. mit Excel bearbeitet werden


## Features:
- legt die CSV Dateien unter dem selben Namen ab wie die input PDFs
- in der output CSV Datei wird das zugehörige Datum und die zugehörigen Brutto Kosten aus der PDF abgelegt

        > Datum;Kosten
