"""
Author: Kai Helferich
Started: 22 May 2020
Last Update: 22 May 2020

version: 1.0
seems to work. not tested with invoices that contain more than parking costs.
"""

import os
import re
import sys
import pdfminer.high_level as miner
from datetime import datetime as dt

INPUTPATH = os.path.join(os.getcwd(), 'input')
OUTPUTPATH = os.path.join(os.getcwd(), 'output')

if not os.path.exists(INPUTPATH):
    print('No input directory found')
    input('Press enter to exit..')
    sys.exit(0)
if not os.path.exists(OUTPUTPATH):
    os.makedirs(OUTPUTPATH)

filelist = list()
summary = dict()

for file in os.scandir(INPUTPATH):
    if file.is_file():
        if file.name.find('.pdf') != -1:
            filelist.append(file)
            # adds every pdf file in the input dir to the set

for file in filelist:
    # iterates through the filelist and saves the text of each file in pdftext
    filename = file.name
    pdftext = miner.extract_text(file.path, codec='utf-8')

    dateoffile = re.search('vom: \d{2}.\d{2}.\d{4}', pdftext)[0]
    dateoffile = dateoffile[5:]  # search for date in the file
    thisfiledates = list()
    thisfilevalues = list()
    thisfiledict = dict()

    pdftext = pdftext.splitlines()
    if 'Parken' in pdftext:
        # the file is probably an important one
        # pdftext_p2 contains only the table of all parking dates and costs
        pdftext_p2 = pdftext[pdftext.index('Seite 1 von 2'):]
        pdftext_p2 = pdftext_p2[pdftext_p2.index('Parken'):pdftext_p2.index('Seite 2 von 2') + 1]
        for item in pdftext_p2:
            date = re.search('\d{2}.\d{2}.\d{4}', item)
            value = re.search('\d,\d{2} €', item)
            if date:
                thisfiledates.append(date[0])
            if value:
                thisfilevalues.append(value[0])

        for item in thisfiledates:  # saves all dates with values in thisfiledict
            index = thisfiledates.index(item)
            thisfiledict[item] = thisfilevalues[index]

        summary[dateoffile] = thisfiledict  # save all entries as subdict in the main dict

outputsummaryfile = open(os.path.join(OUTPUTPATH, 'summary.csv'), 'w')

for date, subdict in summary.items():
    filename = dt.strptime(date, '%d.%m.%Y').strftime('%Y%m%d') + '.csv'
    outputfile = open(os.path.join(OUTPUTPATH, filename), 'w')

    for key, value in subdict.items():
        # writes all values in the single file and the summary file
        toprint = '"{0}";"{1}"\n'.format(key, value)
        outputfile.write(toprint)
        outputsummaryfile.write(toprint)

    outputfile.close()
    print('Created: {}'.format(filename))

outputsummaryfile.close()
print('Created: summary.csv')

input('Press enter to exit..')
